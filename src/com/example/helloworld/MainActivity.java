package com.example.helloworld;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Thread logoTimer = new Thread(){
        	
        	/* Control splash screen time */
        	public void run()
        	{
        		try{
        			int logoTimer = 0;
        			while(logoTimer <3000)
        			{
        				sleep(100);
        				logoTimer+=100;
        			}
        			startActivity(new Intent("com.example.helloworld.CLEAR"));
        		} 
        		
        		catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		finally{
        			finish();
        		}
        	}
        	
        };
        logoTimer.start();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }*/
}
